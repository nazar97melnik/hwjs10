"use strict";
const elementList = document.querySelectorAll(".tabs-content li");
const ulList = document.querySelector(".tabs");
ulList.addEventListener("click", function onClick(e) {
  for (let i = 0; i < ulList.children.length; i++) {
    ulList.children[i].classList.remove("active");
  }
  e.target.classList.add("active");
  if (e.target.className === "tabs-title active") {
    for (let i = 0; i < ulList.children.length; i++) {
      if (e.target === ulList.children[i]) {
        elementList[i].style.display = "block";
      } else {
        elementList[i].style.display = "none";
      }
    }
  }
});
